/*1. Создайте список задач со следующими элементами управления
    * Форма ввода нового элемента (Form input)
    * Кнопка для добавления нового элемента
    * Кнопка для удаления выделенного элемента
    * Кнопки для показа и скрытия выделенных элементов
*/

const root = document.getElementById('root');
const p = document.createElement('p');
const inputTask = document.createElement('input');
const ul = document.createElement('ul');
let count = 1;

p.innerText = "Don't forget to:";
inputTask.type = 'text';
inputTask.placeholder = 'New item here...';

root.append(p, inputTask);

const createButton = (nameButton) => {
    const button = document.createElement('button');
    button.innerHTML = nameButton;
    button.id = nameButton.split(' ')[0].toLowerCase();
    root.append(button);
};

createButton('Add');
createButton('Remove selected');
createButton('Hide selected');
createButton('Show hidden');

const addTasks = () => {
    const li = document.createElement('li');
    const checkbox = document.createElement('input');
    const label = document.createElement('label');

    li.style.listStyleType = 'none';
    checkbox.type = 'checkbox';
    label.innerHTML = inputTask.value;
    checkbox.id = 'num' + count;
    label.setAttribute('for', checkbox.id);
    count++;
    li.append(checkbox, label);
    ul.append(li);
    inputTask.before(ul);
    inputTask.value = '';
};

const removeOrHide = (event) => {
    const checkboxAll = document.querySelectorAll('input');
    const liAll = document.querySelectorAll('li');
    for(let keyCheckbox of checkboxAll) {
        if(keyCheckbox.checked) {
            for(let keyLi of liAll) {
                if(keyCheckbox.parentNode == keyLi) {
                    if(event.target == document.querySelector('#remove')) {
                        keyLi.remove();
                    }
                    if(event.target == document.querySelector('#hide')) {
                        keyLi.style.display = 'none';
                    }
                }
            }
        } 
    }
};

const showSelected = () => {
    const liAll = document.querySelectorAll('li');
    for(let key of liAll) {
        key.style.display = ' block';
    }
};

document.querySelector('#add').addEventListener('click', addTasks);
document.querySelector('#remove').addEventListener('click', removeOrHide);
document.querySelector('#hide').addEventListener('click', removeOrHide);
document.querySelector('#show').addEventListener('click', showSelected);